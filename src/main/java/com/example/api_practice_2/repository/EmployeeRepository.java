package com.example.api_practice_2.repository;
import com.example.api_practice_2.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByEmployeeId(String employeeID);

    boolean existsByEmployeeId(String employeeId);

    int countByDesignation(String designation);
 /*   @Query(value = "SELECT * FROM EMPLOYEE ORDER BY AGE ASC", nativeQuery = true)
    List<Employee> findEmployeeByAgeOrderBy();*/

}
