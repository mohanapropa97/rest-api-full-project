package com.example.api_practice_2.dto;
import com.example.api_practice_2.entity.Employee;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDto {
    private long id;
    private String employeeId;
    private String employeeName;
    private int age;
    private String designation;
}
