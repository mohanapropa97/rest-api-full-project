package com.example.api_practice_2.service;

import com.example.api_practice_2.dto.EmployeeDto;
import com.example.api_practice_2.entity.Employee;
import com.example.api_practice_2.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class EmployeeService {

    public final EmployeeRepository employeeRepository;


    public String saveEmployee(EmployeeDto employeeDto) {
        if (employeeRepository.existsByEmployeeId(employeeDto.getEmployeeId()))
            return "Employee already Exist with this EmployeeID";
        else {
            Employee employee = new Employee();
            BeanUtils.copyProperties(employeeDto, employee);
            employeeRepository.save(employee);
            return "Saved Successfully";
        }
    }

    public List<EmployeeDto> getEmployee() {
        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        for (Employee employee : employeeList) {

            EmployeeDto employeeDto = new EmployeeDto();
            BeanUtils.copyProperties(employee, employeeDto);
            employeeDtoList.add(employeeDto);
        }
        return employeeDtoList;
    }

    public List<EmployeeDto> getEmployeeOrderByAGe() {
        List<Employee> employeeList = employeeRepository.findAll();
        Comparator<Employee> compareByAge = Comparator.comparing(Employee::getAge).reversed();
        Collections.sort(employeeList,compareByAge);
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeDto employeeDto = new EmployeeDto();
            BeanUtils.copyProperties(employee, employeeDto);
            employeeDtoList.add(employeeDto);
        }
        return employeeDtoList;
    }

    public int getEmployeeNumberByDesignation(String designation) {
        int employeeNumber = employeeRepository.countByDesignation(designation);
        return employeeNumber;
    }

    public EmployeeDto getEmployeeById(long id) {
        if (employeeRepository.existsById(id)) {
            Employee employee = employeeRepository.findById(id).get();
            EmployeeDto employeeDto = new EmployeeDto();
            BeanUtils.copyProperties(employee, employeeDto);
            return employeeDto;
        } else {
            return null;
        }

    }

    public EmployeeDto getEmployeeByEmployeeId(String employeeId) {
        Employee employee = employeeRepository.findByEmployeeId(employeeId);
        EmployeeDto employeeDto = new EmployeeDto();
        BeanUtils.copyProperties(employee, employeeDto);
        return employeeDto;
    }
}
