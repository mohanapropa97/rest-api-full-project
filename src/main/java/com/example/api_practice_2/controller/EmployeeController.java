package com.example.api_practice_2.controller;

import com.example.api_practice_2.dto.EmployeeDto;
import com.example.api_practice_2.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
@AllArgsConstructor

public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping("/save")
    public ResponseEntity<?> saveEmployee(@RequestBody EmployeeDto employeeDto) {
        String message = employeeService.saveEmployee(employeeDto);
        //return ResponseEntity.ok(message);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getEmployee() {
        List<EmployeeDto> employeeDtoList = employeeService.getEmployee();
        return new ResponseEntity<>(employeeDtoList, HttpStatus.OK);
    }
    @GetMapping("/getAll/oderByAge")
    public ResponseEntity<?> getEmployeeOrderByAGe()  {
        List<EmployeeDto> employeeDtoList = employeeService.getEmployeeOrderByAGe();
        return new ResponseEntity<>(employeeDtoList, HttpStatus.OK);
    }

    @GetMapping("/count/byDesignation/{designation}")
    public ResponseEntity<?> getEmployeeNumberByDesignation(@PathVariable("designation") String designation) {
        int n = employeeService.getEmployeeNumberByDesignation(designation);
        return new ResponseEntity<>(n, HttpStatus.OK);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<?> getEmployeeById(@PathVariable("id") long id) {
        EmployeeDto employeeDto = employeeService.getEmployeeById(id);
        if (employeeDto != null) {
            return new ResponseEntity<>(employeeDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("No Employee Found By this Id : " + id, HttpStatus.OK);
        }
    }

    @GetMapping("/getByEmployeeId/{employeeId}")
    public ResponseEntity<?> getEmployeeByEmployeeId(@PathVariable("employeeId") String employeeId) {
        EmployeeDto employeeDto = employeeService.getEmployeeByEmployeeId(employeeId);
        return new ResponseEntity<>(employeeDto, HttpStatus.OK);

    }


}
